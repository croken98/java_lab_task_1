package by.zinkov.service;

import by.zinkov.bean.Country;
import by.zinkov.repository.Repository;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.AllCountriesSpecification;
import by.zinkov.repository.impl.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class CountryService {

   private RepositoryFactory repositoryFactory;

    @Autowired
    public CountryService(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    public Optional<Country> add(Country country) {
        Repository<Country> countryRepository = repositoryFactory.getRepository(CountryRepository.class);
        return countryRepository.insert(country);
    }

    public void remove(Country country) {
        Repository<Country> countryRepository = repositoryFactory.getRepository(CountryRepository.class);
        countryRepository.remove(country);
    }

    public void update(Country country) {
        Repository<Country> countryRepository = repositoryFactory.getRepository(CountryRepository.class);
        countryRepository.update(country);
    }

    public Optional<Country> getByPk(int id) {
        Repository<Country> userRepository = repositoryFactory.getRepository(CountryRepository.class);
        return userRepository.getByPK(id);
    }

    public List<Country> getAll() {
        Repository<Country> countryRepository = repositoryFactory.getRepository(CountryRepository.class);
        return countryRepository.query(new AllCountriesSpecification());
    }
}
