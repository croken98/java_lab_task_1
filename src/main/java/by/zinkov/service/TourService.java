package by.zinkov.service;

import by.zinkov.bean.Tour;
import by.zinkov.repository.Repository;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.AllCountriesSpecification;
import by.zinkov.repository.impl.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class TourService {
    private RepositoryFactory repositoryFactory;

    @Autowired
    public TourService(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    public Optional<Tour> add(Tour tour) {
        Repository<Tour> tourRepository = repositoryFactory.getRepository(TourRepository.class);
        return tourRepository.insert(tour);
    }

    public void remove(Tour tour) {
        Repository<Tour> tourRepository = repositoryFactory.getRepository(TourRepository.class);
        tourRepository.remove(tour);
    }

    public void update(Tour tour) {
        Repository<Tour> tourRepository = repositoryFactory.getRepository(TourRepository.class);
        tourRepository.update(tour);
    }

    public Optional<Tour> getByPk(int id) {
        Repository<Tour> userRepository = repositoryFactory.getRepository(TourRepository.class);
        return userRepository.getByPK(id);
    }

    public List<Tour> getAll() {
        Repository<Tour> tourRepository = repositoryFactory.getRepository(TourRepository.class);
        return tourRepository.query(new AllCountriesSpecification());
    }
}
