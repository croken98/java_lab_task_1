package by.zinkov.service;

import by.zinkov.bean.User;
import by.zinkov.repository.Repository;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.AllUsersSpecification;
import by.zinkov.repository.impl.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;


@Component
public class UserService {

    RepositoryFactory repositoryFactory;

    @Autowired
    public UserService(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    public Optional<User> addUser(User user) {
        Repository<User> userRepository = repositoryFactory.getRepository(UserRepository.class);
        return userRepository.insert(user);
    }

    public void remove(User user) {
        Repository<User> userRepository = repositoryFactory.getRepository(UserRepository.class);
        userRepository.remove(user);
    }

    public void update(User user) {
        Repository<User> userRepository = repositoryFactory.getRepository(UserRepository.class);
        userRepository.update(user);
    }

    public Optional<User> getByPk(int id) {
        Repository<User> userRepository = repositoryFactory.getRepository(UserRepository.class);
        return userRepository.getByPK(id);
    }

    public List<User> getAllUsers() {
        Repository<User> userRepository = repositoryFactory.getRepository(UserRepository.class);
        return userRepository.query(new AllUsersSpecification());
    }
}
