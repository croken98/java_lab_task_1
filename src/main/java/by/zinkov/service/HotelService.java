package by.zinkov.service;

import by.zinkov.bean.Hotel;
import by.zinkov.repository.Repository;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.AllCountriesSpecification;
import by.zinkov.repository.impl.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class HotelService {

   private RepositoryFactory repositoryFactory;

    @Autowired
    public HotelService(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    public Optional<Hotel> add(Hotel hotel) {
        Repository<Hotel> hotelRepository = repositoryFactory.getRepository(HotelRepository.class);
        return hotelRepository.insert(hotel);
    }

    public void remove(Hotel hotel) {
        Repository<Hotel> hotelRepository = repositoryFactory.getRepository(HotelRepository.class);
        hotelRepository.remove(hotel);
    }

    public void update(Hotel hotel) {
        Repository<Hotel> hotelRepository = repositoryFactory.getRepository(HotelRepository.class);
        hotelRepository.update(hotel);
    }

    public Optional<Hotel> getByPk(int id) {
        Repository<Hotel> userRepository = repositoryFactory.getRepository(HotelRepository.class);
        return userRepository.getByPK(id);
    }

    public List<Hotel> getAll() {
        Repository<Hotel> hotelRepository = repositoryFactory.getRepository(HotelRepository.class);
        return hotelRepository.query(new AllCountriesSpecification());
    }

}
