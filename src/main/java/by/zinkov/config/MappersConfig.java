package by.zinkov.config;

import by.zinkov.bean.Country;
import by.zinkov.bean.Review;
import by.zinkov.bean.User;
import by.zinkov.bean.UserTour;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
@Configuration
@ComponentScan("by.zinkov.repository.mapper")
public class MappersConfig {
    @Bean
    public RowMapper<Country> countryRowMapper(){
        return new BeanPropertyRowMapper<>(Country.class);
    }

    @Bean
    public RowMapper<Review> reviewRowMapper() {
        return new BeanPropertyRowMapper<>(Review.class);
    }

    @Bean
    public RowMapper<User> userRowMapper(){
        return new BeanPropertyRowMapper<>(User.class);
    }

    @Bean
    public RowMapper<UserTour> userTourRowMapper(){
        return new BeanPropertyRowMapper<>(UserTour.class);
    }
}
