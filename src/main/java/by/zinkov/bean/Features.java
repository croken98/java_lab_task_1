package by.zinkov.bean;

import java.util.Arrays;
import java.util.Optional;

public enum Features {

    FREE_WIFI("free wifi"),
    WINE_BAR("Wine Bar"),
    BILLIARD_TABLE("Billiards Table"),
    INDOOR_HOTEL_POOL("Indoor Heated Pool"),
    FITNESS_CENTER("Fitness Center"),
    ROOFTOP_ABSEILING("Rooftop Abseiling"),
    BUSINESS_SERVICES("Business Services"),
    TRANSPORTATION_AND_PARKING("Transportation & Parking"),
    FREE_DRINKS("Free drinks"),
    GUEST_SERVICES("Guest Services");

    private String name;

    Features(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Features getByName(String name){
        Optional<Features> features = Arrays.stream(Features.values()).filter(item->item.name.equals(name)).findFirst();
        return features.orElse(null);
    }

}
