package by.zinkov.bean;

import java.util.Arrays;
import java.util.Objects;

public class Hotel {
    private int id;
    private String name;
    private byte stars;
    private String website;
    private String lalitude;
    private String longitude;
    private Features[] features;

    @Override
    public String toString() {
        return "Hotel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", stars=" + stars +
                ", website='" + website + '\'' +
                ", lalitude='" + lalitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", features=" + Arrays.toString(features) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return id == hotel.id &&
                stars == hotel.stars &&
                Objects.equals(name, hotel.name) &&
                Objects.equals(website, hotel.website) &&
                Objects.equals(lalitude, hotel.lalitude) &&
                Objects.equals(longitude, hotel.longitude) &&
                Arrays.equals(features, hotel.features);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, name, stars, website, lalitude, longitude);
        result = 31 * result + Arrays.hashCode(features);
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getStars() {
        return stars;
    }

    public void setStars(byte stars) {
        this.stars = stars;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLalitude() {
        return lalitude;
    }

    public void setLalitude(String lalitude) {
        this.lalitude = lalitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Features[] getFeatures() {
        return features;
    }

    public void setFeatures(Features[] features) {
        this.features = features;
    }
}
