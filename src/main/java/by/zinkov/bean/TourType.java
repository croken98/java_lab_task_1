package by.zinkov.bean;

import java.util.Arrays;
import java.util.Optional;

public enum TourType {
    EVENT_TRAVEL("Event Travel"),
    VISITING_FRIENDS_OR_RELATIVES("Visiting Friends or Relatives"),
    BUSINESS_TRAVEL("Business Travel"),
    THE_GAP_YEAR("The Gap Year"),
    LONG_TERM_SLOW_TRAVEL("Long Term Slow Travel"),
    VOLUNTEER_TRAVEL("Volunteer Travel"),
    THE_CARAVAN_OR_RV_ROAD_TRIP("The Caravan/RV Road Trip"),
    THE_GROUP_TOUR("The Group Tour"),
    THE_PACKAGE_HOLIDAY("The Package Holiday"),
    THE_WEEKEND_BREAK("The Weekend Break");


    private String name;

    TourType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static TourType getByName(String name) {
        Optional<TourType> tourType = Arrays.stream(TourType.values()).filter(item -> item.name.equals(name)).findFirst();
        return tourType.orElse(null);
    }
}
