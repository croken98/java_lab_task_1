package by.zinkov.bean;

import java.util.Objects;

public class UserTour {
    int userId;
    int tourId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTourId() {
        return tourId;
    }

    public void setTourId(int tourId) {
        this.tourId = tourId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserTour userTour = (UserTour) o;
        return userId == userTour.userId &&
                tourId == userTour.tourId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, tourId);
    }

    @Override
    public String toString() {
        return "UserTour{" +
                "userId=" + userId +
                ", tourId=" + tourId +
                '}';
    }
}
