package by.zinkov.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Objects;

public class Tour {
    private int id;
    private byte[] photo;
    private Timestamp date;
    private long duration;
    private String description;
    private BigDecimal cost;
    private TourType tour;
    private int hotelId;
    private int countryId;

    @Override
    public String toString() {
        return "Tour{" +
                "id=" + id +
                ", photo=" + Arrays.toString(photo) +
                ", date=" + date +
                ", duration=" + duration +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                ", tour=" + tour +
                ", hotelId=" + hotelId +
                ", countryId=" + countryId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tour tour1 = (Tour) o;
        return id == tour1.id &&
                duration == tour1.duration &&
                hotelId == tour1.hotelId &&
                countryId == tour1.countryId &&
                Arrays.equals(photo, tour1.photo) &&
                Objects.equals(date, tour1.date) &&
                Objects.equals(description, tour1.description) &&
                Objects.equals(cost, tour1.cost) &&
                tour == tour1.tour;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, date, duration, description, cost, tour, hotelId, countryId);
        result = 31 * result + Arrays.hashCode(photo);
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public TourType getTour() {
        return tour;
    }

    public void setTour(TourType tour) {
        this.tour = tour;
    }

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}
