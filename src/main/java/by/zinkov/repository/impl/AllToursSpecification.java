package by.zinkov.repository.impl;

import by.zinkov.repository.Specification;
import org.springframework.stereotype.Component;

@Component
public class AllToursSpecification implements Specification {
    @Override
    public String toSql() {
        return "SELECT * FROM tour;";
    }

    @Override
    public Object[] getArgs() {
        return new Object[0];
    }
}
