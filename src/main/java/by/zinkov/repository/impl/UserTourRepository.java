package by.zinkov.repository.impl;

import by.zinkov.bean.UserTour;
import by.zinkov.repository.AbstractRepository;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@Component("userTourRepository")

public class UserTourRepository extends AbstractRepository<UserTour> {

private static final String SELECT_BY_TOUR_AND_USER_TOUR_ID = "SELECT *  FROM user_tour WHERE user_id = ? and tour_id=?;";
    @Override
    public Optional<UserTour> insert(UserTour object) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(getCreateQuery(), new String[]{ "user_id","tour_id"});
            setterStatement(ps, object);
            return ps;
        }, keyHolder);

        UserTour userTour = new UserTour();
        userTour.setUserId((Integer) keyHolder.getKeys().get("user_id"));
        userTour.setTourId((Integer) keyHolder.getKeys().get("tour_id"));

        return getByEntity(userTour);
    }

    public Optional<UserTour> getByEntity(UserTour userTour) {
        List<UserTour> objectList = jdbcTemplate.query(getSelectByKeys(), new Object[]{userTour.getUserId(), userTour.getTourId()}, rowMapper);
        return objectList.isEmpty() ? Optional.empty() : Optional.of(objectList.get(0));
    }

    private String getSelectByKeys() {
        return SELECT_BY_TOUR_AND_USER_TOUR_ID;
    }

    @Override
    public Optional<UserTour> getByPK(int pk) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void setterStatement(PreparedStatement statement, UserTour object) throws SQLException {
        statement.setInt(1, object.getUserId());
        statement.setInt(2, object.getTourId());
    }

    @Override
    protected String getSelectByPKQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO user_tour(user_id, tour_id) VALUES(? , ? )";
    }

    @Override
    protected Object[] getPreparedStatementSetterForAdd(UserTour object) {
        Object[] statementSetter = new Object[2];
        statementSetter[0] = object.getUserId();
        statementSetter[1] = object.getTourId();
        return statementSetter;
    }


    @Override
    protected String getUpdateQuery() {
        return "UPDATE user_tour SET user_id = ?, tour_id = ? WHERE user_id = ? and tour_id=? ;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForUpdate(UserTour object) {
        Object[] statementSetter = new Object[4];
        statementSetter[0] = object.getUserId();
        statementSetter[1] = object.getTourId();
        statementSetter[2] = object.getUserId();
        statementSetter[3] = object.getTourId();
        return statementSetter;
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM user_tour WHERE user_id = ? and tour_id=? ;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForDelete(UserTour object) {
       return getPreparedStatementSetterForAdd(object);
    }
}
