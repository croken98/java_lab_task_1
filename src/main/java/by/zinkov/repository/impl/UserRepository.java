package by.zinkov.repository.impl;

import by.zinkov.bean.User;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component("userRepository")
public class UserRepository extends AbstractRepository<User> {

    @Override
    protected void setterStatement(PreparedStatement statement, User object) throws SQLException {
        statement.setString(1,object.getLogin());
        statement.setString(2,object.getPassword());
    }

    @Override
    protected String getSelectByPKQuery() {
        return "SELECT * FROM user_travel_agency WHERE id = ?";

    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO user_travel_agency(login, password) VALUES(? , ? )";
    }

    @Override
    protected Object[] getPreparedStatementSetterForAdd(User object) {
        Object[] statementSetter =  new Object[2];
        statementSetter[0] = object.getLogin();
        statementSetter[1] = object.getPassword();
        return statementSetter;
    }



    @Override
    protected String getUpdateQuery() {
        return "UPDATE user_travel_agency SET id = ?, login = ?, password = ? WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForUpdate(User object) {
        Object[] statementSetter =  new Object[4];
        statementSetter[0] = object.getId();
        statementSetter[1] = object.getLogin();
        statementSetter[2] = object.getPassword();
        statementSetter[3] = object.getId();
        return statementSetter;
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM user_travel_agency WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForDelete(User object) {
        Object[] statementSetter =  new Object[1];
        statementSetter[0] = object.getId();
        return statementSetter;
    }
}
