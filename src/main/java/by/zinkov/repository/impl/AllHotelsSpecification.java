package by.zinkov.repository.impl;

import by.zinkov.repository.Specification;
import org.springframework.stereotype.Component;

@Component
public class AllHotelsSpecification implements Specification {
    @Override
    public String toSql() {
        return "SELECT * FROM hotel";
    }

    @Override
    public Object[] getArgs() {
        return new Object[0];
    }
}
