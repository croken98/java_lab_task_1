package by.zinkov.repository.impl;

import by.zinkov.repository.Specification;
import org.springframework.stereotype.Component;

@Component
public class AllUsersToursSpecification implements Specification {
    @Override
    public String toSql() {
        return "SELECT * FROM user_tour;";
    }

    @Override
    public Object[] getArgs() {
        return new Object[0];
    }
}
