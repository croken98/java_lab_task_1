package by.zinkov.repository.impl;

import by.zinkov.bean.Review;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;


@Component("reviewRepository")
public class ReviewRepository extends AbstractRepository<Review> {


    @Override
    protected void setterStatement(PreparedStatement statement, Review object) throws SQLException {
        statement.setTimestamp(1, object.getDate());
        statement.setString(2, object.getText());
        statement.setInt(3, object.getUserId());
        statement.setInt(4, object.getTourId());
    }

    @Override
    protected String getSelectByPKQuery() {
        return "SELECT * FROM review WHERE id = ?";

    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO review(date, text, userId , tourId) VALUES(? , ?, ? , ? )";
    }

    @Override
    protected Object[] getPreparedStatementSetterForAdd(Review object) {
        Object[] statementSetter = new Object[4];
        statementSetter[0] = object.getDate();
        statementSetter[1] = object.getText();
        statementSetter[2] = object.getUserId();
        statementSetter[3] = object.getTourId();
        return statementSetter;
    }


    @Override
    protected String getUpdateQuery() {
        return "UPDATE review SET date = ?, text = ?, userId = ? , tourId = ? WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForUpdate(Review object) {
        Object[] statementSetter = new Object[5];
        statementSetter[0] = object.getDate();
        statementSetter[1] = object.getText();
        statementSetter[2] = object.getUserId();
        statementSetter[3] = object.getTourId();
        statementSetter[4] = object.getId();
        return statementSetter;
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM review WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForDelete(Review object) {
        Object[] statementSetter = new Object[1];
        statementSetter[0] = object.getId();
        return statementSetter;
    }

}
