package by.zinkov.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class RepositoryFactory {
    @Autowired
    ApplicationContext applicationContext;
    public <T> Repository<T> getRepository(Class<? extends Repository<T>> beanClass){
        return applicationContext.getBean(beanClass);
    }

}
