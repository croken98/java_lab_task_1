package by.zinkov.repository.mapper;

import by.zinkov.bean.Features;
import by.zinkov.bean.Hotel;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

@Component
public class HotelMapper implements RowMapper<Hotel> {
    @Override
    public Hotel mapRow(ResultSet resultSet, int i) throws SQLException {
        Hotel hotel = new Hotel();
        hotel.setId(resultSet.getInt("id"));
        hotel.setName(resultSet.getString("name"));
        hotel.setStars(resultSet.getByte("stars"));
        hotel.setWebsite(resultSet.getString("website"));
        hotel.setLalitude(resultSet.getString("lalitude"));
        hotel.setLongitude(resultSet.getString("longitude"));
        Array features = resultSet.getArray("features");
        String[] featuresInStringArr = (String[]) features.getArray();
        Object[] featuresObjectArray = Arrays.stream(featuresInStringArr).map(Features::getByName).toArray();
        Features[] featuresArray = new Features[featuresObjectArray.length];
        for(int j = 0 ; j < featuresObjectArray.length ; ++j){
            featuresArray[j] = (Features) featuresObjectArray[j];
        }
        hotel.setFeatures(featuresArray);
        return hotel;
    }
}
