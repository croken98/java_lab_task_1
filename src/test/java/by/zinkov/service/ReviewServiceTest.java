package by.zinkov.service;

import by.zinkov.bean.Review;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.ReviewRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReviewServiceTest {

    @Mock
    private RepositoryFactory repositoryFactoryMock;

    @Mock
    private ReviewRepository reviewRepositoryMock;

    @InjectMocks
    private ReviewService reviewService;


    @Before
    public void usualMocks() {
        when(repositoryFactoryMock.getRepository(ReviewRepository.class)).thenReturn(reviewRepositoryMock);
    }

    @Test
    public void whenCallInsertReturnOptionalWithId() {

        Review reviewOne = new Review();
        reviewOne.setText("BY");

        Review reviewTwo = new Review();
        reviewTwo.setText("BY");
        reviewTwo.setId(1);
        when(reviewRepositoryMock.insert(reviewOne)).thenReturn(Optional.of(reviewTwo));
        Optional<Review> optionalReview = reviewService.add(reviewOne);
        Mockito.verify(reviewRepositoryMock, Mockito.times(1)).insert(reviewOne);

        Assert.assertEquals(optionalReview, Optional.of(reviewTwo));

    }

    @Test
    public void removeTest() {
        Review review = new Review();
        review.setText("BY");
        review.setId(1);
        reviewService.remove(review);
        Mockito.verify(reviewRepositoryMock, Mockito.times(1)).remove(review);
    }

    @Test
    public void updateTest() {
        Review review = new Review();
        review.setText("BY");
        review.setId(1);
        reviewService.update(review);
        Mockito.verify(reviewRepositoryMock, Mockito.times(1)).update(review);
    }

    @Test
    public void whenCallGetByPKReturnOptional() {
        Review review = new Review();
        review.setText("BY");
        review.setId(1);
        when(reviewRepositoryMock.getByPK(review.getId())).thenReturn(Optional.of(review));
        Optional<Review> optionalReview = reviewService.getByPk(review.getId());
        Mockito.verify(reviewRepositoryMock, Mockito.times(1)).getByPK(review.getId());
        Assert.assertEquals(review, optionalReview.get());
    }

}