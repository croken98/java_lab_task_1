package by.zinkov.service;

import by.zinkov.bean.User;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private RepositoryFactory repositoryFactoryMock;

    @Mock
    private UserRepository userRepositoryMock;

    @InjectMocks
    private UserService userService;


    @Before
    public void usualMocks() {
        when(repositoryFactoryMock.getRepository(UserRepository.class)).thenReturn(userRepositoryMock);
    }

    @Test
    public void whenCallInsertReturnOptionalWithId() {

        User userOne = new User();
        userOne.setLogin("BY");

        User userTwo = new User();
        userTwo.setLogin("BY");
        userTwo.setId(1);
        when(userRepositoryMock.insert(userOne)).thenReturn(Optional.of(userTwo));
        Optional<User> optionalUser = userService.addUser(userOne);
        Mockito.verify(userRepositoryMock, Mockito.times(1)).insert(userOne);
        Assert.assertEquals(optionalUser, Optional.of(userTwo));
    }

    @Test
    public void removeTest() {
        User user = new User();
        user.setLogin("BY");
        user.setId(1);
        userService.remove(user);
        Mockito.verify(userRepositoryMock, Mockito.times(1)).remove(user);
    }

    @Test
    public void updateTest() {
        User user = new User();
        user.setLogin("BY");
        user.setId(1);
        userService.update(user);
        Mockito.verify(userRepositoryMock, Mockito.times(1)).update(user);
    }

    @Test
    public void whenCallGetByPKReturnOptional() {
        User user = new User();
        user.setLogin("BY");
        user.setId(1);
        when(userRepositoryMock.getByPK(user.getId())).thenReturn(Optional.of(user));
        Optional<User> optionalUser = userService.getByPk(user.getId());
        Mockito.verify(userRepositoryMock, Mockito.times(1)).getByPK(user.getId());
        Assert.assertEquals(user, optionalUser.get());
    }
}