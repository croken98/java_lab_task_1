package by.zinkov.service;

import by.zinkov.bean.Hotel;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.HotelRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HotelServiceTest {

    @Mock
    private RepositoryFactory repositoryFactoryMock;

    @Mock
    private HotelRepository hotelRepositoryMock;

    @InjectMocks
    private HotelService hotelService;


    @Before
    public void usualMocks() {
        when(repositoryFactoryMock.getRepository(HotelRepository.class)).thenReturn(hotelRepositoryMock);
    }

    @Test
    public void whenCallInsertReturnOptionalWithId() {

        Hotel hotelOne = new Hotel();
        hotelOne.setName("BY");
        Hotel hotelTwo = new Hotel();
        hotelTwo.setName("BY");
        hotelTwo.setId(1);
        when(hotelRepositoryMock.insert(hotelOne)).thenReturn(Optional.of(hotelTwo));
        Optional<Hotel> optionalHotel = hotelService.add(hotelOne);
        Mockito.verify(hotelRepositoryMock, Mockito.times(1)).insert(hotelOne);
        Assert.assertEquals(optionalHotel, Optional.of(hotelTwo));
    }

    @Test
    public void removeTest() {
        Hotel hotel = new Hotel();
        hotel.setName("BY");
        hotel.setId(1);
        hotelService.remove(hotel);
        Mockito.verify(hotelRepositoryMock, Mockito.times(1)).remove(hotel);
    }

    @Test
    public void updateTest() {
        Hotel hotel = new Hotel();
        hotel.setName("BY");
        hotel.setId(1);
        hotelService.update(hotel);
        Mockito.verify(hotelRepositoryMock, Mockito.times(1)).update(hotel);
    }

    @Test
    public void whenCallGetByPKReturnOptional() {
        Hotel hotel = new Hotel();
        hotel.setName("BY");
        hotel.setId(1);
        when(hotelRepositoryMock.getByPK(hotel.getId())).thenReturn(Optional.of(hotel));
        Optional<Hotel> optionalHotel = hotelService.getByPk(hotel.getId());
        Mockito.verify(hotelRepositoryMock, Mockito.times(1)).getByPK(hotel.getId());
        Assert.assertEquals(hotel, optionalHotel.get());
    }
}