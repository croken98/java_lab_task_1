package by.zinkov.repository.impl;

import by.zinkov.config.DBConfigForTests;
import by.zinkov.config.MappersConfig;
import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class, MappersConfig.class})
public class AbstractTest {
    @Autowired
    private Flyway flyway;

    @Before
    public void migrate() {
        flyway.migrate();
    }

    @After
    public void clean() {
        flyway.clean();
    }

    @Test
    public void defaultTest() {
        Assert.assertTrue(true);
    }
}
